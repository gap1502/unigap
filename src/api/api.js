import http from "@/api/http";

export const login = (username, password) => {
    return http.post(
        '/login',
        {
            login: username,
            password
        }
    );
}

export const register = (username, password) => {
    return http.post('/register', {
        username: username,
        password: password,
    });
}



