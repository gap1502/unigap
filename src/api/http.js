import axios from "axios";
import cookies from 'browser-cookies';

const AUTH_TOKEN_KEY = 'auth-token';

const http = axios.create({
    baseURL: process.env.VUE_APP_BASE_URL,
    headers: {
        "Content-Type": "application/json",
    },
    withCredentials: true
});

const getAuthToken = () => cookies.get(AUTH_TOKEN_KEY);

const authInterceptor = (config) => {
    config.mode =  'no-cors';

    if (getAuthToken() !== null) {
        config.headers[AUTH_TOKEN_KEY] = `Bearer ${getAuthToken()}`;
    }

    return config;
}

export function setupHttpInterceptors(store, router) {
    http.interceptors.request.use(authInterceptor);

    http.interceptors.response.use(r => r, function (error) {
        if (error.response.status === 401 && error.response.config.url !== '/logout') {
            store.dispatch('logout')
                .then(() => {
                    router.push({
                        name: 'Login'
                    });
                })

            console.error('Авторизация на фронтенде удалена');
        }
        return Promise.reject(error);
    });
}

export default http;

export {
    AUTH_TOKEN_KEY
};

