import { createApp } from 'vue'
import App from './App.vue'

import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import {setupHttpInterceptors} from "@/api/http";

import router from './router/index'

import store from './store/index'

const vuetify = createVuetify({
    components,
    directives,
})

setupHttpInterceptors(store, router)

createApp(App).use(vuetify).use(router).use(store).mount('#app')
