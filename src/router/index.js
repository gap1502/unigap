import {createRouter, createWebHistory} from "vue-router";
import SingUp from "@/views/SingUp";
import Notifications from "@/components/Notifications";
import Login from "@/views/Login";
import Home from "@/views/Home";
import store from "../store/index"


const routes = [
    {
        path: '/',
        name: 'notifications',
        component: Notifications
    },
    {
        path: '/sing',
        name: 'singUp',
        component: SingUp,
        meta: {
            guestOnly: true
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            guestOnly: true
        }
    },
    {
        path: '/home',
        name: 'Home',
        component: Home,
        meta: {
            authorizedOnly: true
        }
    },
    {
        path: "/:pathMatch(.*)*",
        redirect: {
            name: 'Login'
        }
    }

]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

router.beforeEach((to, from, next) => {
    if (to.meta && to.meta.authorizedOnly && store.getters.isGuest) {

        router.push({
            name: 'Login'
        });

        return;
    }

    if (to.meta) {

        if (to.meta.guestOnly && !store.getters.isGuest) {

            router.push({
                name: 'Home'
            });

            return;
        }
    }

    next();
});



export default router;
