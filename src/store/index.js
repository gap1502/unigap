import { createStore } from "vuex";
import cookies from 'browser-cookies';
import { AUTH_TOKEN_KEY } from "@/api/http";
import { login, register } from "@/api/api";

export default createStore({
    state() {
        return {
            authToken: cookies.get(AUTH_TOKEN_KEY),
            user: null
        }
    },
    mutations: {
        authToken(state, val) {
            state.authToken = val
        },
        setUser(state, user) {
            state.user = user
        }
    },
    getters: {
        isGuest(state) {
            return !state.authToken;
        },
        isLoggedIn: state => state.user !== null
    },
    actions: {
        setAuthToken({ commit }, token) {
            commit('authToken', token);
            cookies.set(AUTH_TOKEN_KEY, token);
        },
        login({ dispatch }, { username, password }) {
            return login(username, password)
                .then(res => {
                    dispatch('setAuthToken', res.data['auth-token']);
                    return res;
                })
                .catch(err => {
                    if (err.response) {
                        throw err.response;
                    } else {
                        throw err;
                    }
                })
        },
        register({ dispatch }, { username, password }) {
            return register(username, password)
                .then(res => {
                    dispatch('setUser', res.data);
                    return res;
                })
                .catch(err => {
                    if (err.response) {
                        throw err.response;
                    } else {
                        throw err;
                    }
                });
        }
    }
})

